import React from 'react'
import {Provider} from 'react-redux'
import ReactDOM from 'react-dom'
import AppRouter from './routers/AppRouter'
import 'normalize.css/normalize.css'
import './styles/styles.scss'
import configureStore from './store/configureStore'
import {addExpense} from './actions/expenses'
import {setTextFilter} from './actions/filters'
import getVisibleExpenses from './selectors/expenses'

const store = configureStore()


store.dispatch(addExpense({description: "Water bill", amount: 4500}))
store.dispatch(addExpense({description: "gass bill", craetedAt: 1000}))
store.dispatch(addExpense({description: "rent bill", amount: 19500}))
store.dispatch(setTextFilter('bill'))




const state = store.getState();
const visibleExpenses = getVisibleExpenses(state.expenses, state.filters)
console.log(visibleExpenses)
store.subscribe(()=> {
    const state = store.getState();
    console.log(state.filters)    
    const ve = getVisibleExpenses(state.expenses, state.filters)
    console.log(ve)
})

const jsx = (
    <Provider store={store}>
        <AppRouter />    
    </Provider>
);

ReactDOM.render(jsx, document.getElementById('app'));