import React from 'react'
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import ExpenseDashboardPage from '../components/ExpenseDashboardPage'
import AddExpensePage from '../components/AddExpensePage'
import EditExpensePage from '../components/EditExpensePage'
import Header from '../components/Header'
import HelpPage from '../components/HelpPage'
import NotFountPage from '../components/NotFountPage'








const AppRouter = ()=> (
    <BrowserRouter>
        <div>
            <Header/>
            <Switch>
                   <Route path="/" component={ExpenseDashboardPage} exact={true}/>
                   <Route path="/create"  component={AddExpensePage} />
                   <Route path="/edit/:id/:a?"  component={EditExpensePage} />
                   <Route path="/help"  component={HelpPage} />
                <Route component={NotFountPage} />
            </Switch>
        </div>
    </BrowserRouter>
)

export default AppRouter