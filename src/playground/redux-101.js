import {createStore} from 'redux'

const incrementCount = ({incrementBy = 1} = {})=> {
    return {
        type: 'INCREMENT',
        incrementBy
    }
}

const decrementCount = ({decrementBy = 1}={}) => ({
    type: 'DECREMENT',
     decrementBy
});

const setCount = ({count}) => ({
    type: 'SET',
     count
});

const reset = () => ({
    type: 'RESET'
});

//Reducers
//1. Reducer are pure function - output is only determined by input, 
//      it doesn't use anything else from outside the function scope and does not change anytning
//2. Never change state or action

const countReducer = (state = {count: 0}, action)=> {

    switch(action.type){
        case 'INCREMENT':
            return {count: state.count + action.incrementBy}
        case 'DECREMENT':
            return {count: state.count - action.decrementBy}
        case 'RESET':
            return {count: 0}
        case 'SET':
            return {count: action.count}
        default:
            return state
    }
}

const store = createStore(countReducer);

const unsubscribe = store.subscribe(()=>{
    console.log( store.getState())
});


store.dispatch(incrementCount({incrementBy: 5}));
//unsubscribe();

store.dispatch(incrementCount());

store.dispatch(reset());

store.dispatch(decrementCount());

store.dispatch(decrementCount({decrementBy: 10}));

store.dispatch(setCount({count: 101}));
